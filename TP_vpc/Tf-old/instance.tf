resource "scaleway_instance_server" "srv1" {
    name  = "Cloud8-srv1"
    type  = "PRO2-XXS"
    image = "debian_bookworm"
    tags = [ "Cloud8", "web", "apache2" ]
    root_volume {
      size_in_gb = 10
    }
    user_data = {
       role       = "web"
       cloud-init = file("${path.module}/init_srv1.sh")
    }
    enable_ipv6 = false
    private_network {
       pn_id = scaleway_vpc_private_network.myvpc.id
    }
}

# Seconde instance

resource "scaleway_instance_server" "srv2" {
    name  = "Cloud8-srv2"
    type  = "PRO2-XXS"
    image = "debian_bookworm"
    tags = [ "Cloud8", "web", "apache2" ]
    root_volume {
      size_in_gb = 10
    }
    user_data = {
       role       = "web"
       cloud-init = file("${path.module}/init_srv2.sh")
    }
    enable_ipv6 = false
    private_network {
       pn_id = scaleway_vpc_private_network.myvpc.id
    }
}

resource "scaleway_instance_server" "srv3" {
    name  = "Cloud8-srv3"
    type  = "PRO2-XXS"
    image = "debian_bookworm"
    tags = [ "Cloud8", "web", "apache2" ]
    root_volume {
      size_in_gb = 10
    }
    user_data = {
       role       = "web"
       cloud-init = file("${path.module}/init_srv3.sh")
    }
    enable_ipv6 = false
    private_network {
       pn_id = scaleway_vpc_private_network.myvpc.id
    }
}



# Données en sortie

output "srv1_private_ip" {
  value = "${scaleway_instance_server.srv1.private_ip}"
}

output "srv2_private_ip" {
  value = "${scaleway_instance_server.srv2.private_ip}"
}

output "srv3_private_ip" {
  value = "${scaleway_instance_server.srv3.private_ip}"
}
