#resource "scaleway_instance_ip" "public_ip1" {
#  project_id = var.project_id
#}

#resource "scaleway_instance_ip" "public_ip2" {
#  project_id = var.project_id
#}

resource "scaleway_instance_server" "web1" {
  name = "${local.team}-web1"

  project_id = var.project_id
  type       = "PRO2-XXS"
  image      = "debian_bullseye"

  tags = ["front", "web"]

 # ip_id = scaleway_instance_ip.public_ip1.id

  root_volume {
    # The local storage of a DEV1-L instance is 80 GB, subtract 30 GB from the additional l_ssd volume, then the root volume needs to be 50 GB.
    size_in_gb = 10
  }

  user_data = {
    name        = "web1"
    myip        = "192.168.42.111"
    cloud-init = file("${path.module}/init_instance.sh")
    #cloud-init = file("${path.module}/deploy-wp")
    # "web1ip"   = "${scaleway_instance_server.web1.public_ip}"
  }



  #security_group_id = scaleway_instance_security_group.sg-www.id

  private_network {
    pn_id = scaleway_vpc_private_network.myvpc.id
  }

}

resource "scaleway_instance_server" "web2" {
  name = "${local.team}-web2"

  project_id = var.project_id
  type       = "PRO2-XXS"
  image      = "debian_bullseye"

  tags = ["front", "web"]
  
  user_data = {
    name        = "web2"
    myip        = "192.168.42.112"
    cloud-init = file("${path.module}/init_instance.sh")
    #cloud-init = file("${path.module}/deploy-wp")
    # "web1ip"   = "${scaleway_instance_server.web1.public_ip}"
  }

  #depends_on =  [ scaleway_instance_server.web1 ]
  #ip_id = scaleway_instance_ip.public_ip2.id

  #additional_volume_ids = [scaleway_instance_volume.data.id]

  root_volume {
    # The local storage of a DEV1-L instance is 80 GB, subtract 30 GB from the additional l_ssd volume, then the root volume needs to be 50 GB.
    size_in_gb = 10
  }

  #security_group_id = scaleway_instance_security_group.sg-www.id
  
  private_network {
    pn_id = scaleway_vpc_private_network.myvpc.id
  }

}

#resource scaleway_instance_private_nic "nic1" {
#  private_network_id = scaleway_vpc_private_network.myvpc.id
#  server_id          = scaleway_instance_server.web1.id
#}

#resource scaleway_instance_private_nic "nic2" {
#  private_network_id = scaleway_vpc_private_network.myvpc.id
#  server_id          = scaleway_instance_server.web2.id
#}

#output "web1_ip" {
#  value = "${scaleway_instance_server.web1.public_ip}"
#}

#output "web2_ip" {
#  value = "${scaleway_instance_server.web2.public_ip}"
#}
