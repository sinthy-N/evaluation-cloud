# Partie 1 :

Rapport sur le Projet "Évaluations”


1. Configuration GitLab

La première phase du projet a débuté par la création d'un nouveau projet public, nommé "Évaluations", sur GitLab. Ceci a impliqué le clonage du dépôt existant "3-clou-v-23-jp" et l'association avec le compte GitLab de "yosr88". Les commandes suivantes ont été utilisées pour effectuer cette configuration :

Commande :
cd ~/3clou-v23/3-clou-v-23-jp
git remote remove origin
git remote add origin https://gitlab.com/yosr88/evaluation.git
git config --global user.name "yosr88"
git config --global user.email "yosr.trabelsi@ecole-hexagone.com"
git add .
git commit -m "Initial commit of 3-clou-v-23-jp project"
git push --set-upstream origin main
<br>
<img src="images/allez-sur-eval.png" width="300"/>
<img src="images/Eval.png" width="300"/>
<br>

2. Création de la Première Instance sur Scaleway

La seconde étape a impliqué la création de la première instance, baptisée "Cloud8_srv1", sur Scaleway. OpenTofu, utilisé à la place de Terraform, a été employé pour cette tâche. Les commandes clés sont les suivantes :
<br>
<img src="images/instance-1-script-nano.png" width="300"/>
<img src="images/Instance-creer.png" width="300"/>
<br>

- Initialisation de OpenTofu :
Commande :
  tofu init

- Validation et Résumé du Déploiement :
Commande :
  tofu plan

- Application du Déploiement :
Commande :
  tofu apply


3. Analyse du Plan de Déploiement

La commande `tofu plan` a révélé des changements externes au plan initial, indiquant que l'adresse IP publique et le serveur Scaleway "Cloud8_srv1" avaient été supprimés en dehors de OpenTofu. Le plan résultant prévoyait donc la création de ces ressources.


5. Application du Plan de Déploiement

Après validation du plan avec `tofu apply`, OpenTofu a recréé avec succès l'adresse IP publique et le serveur Scaleway "Cloud8_srv1". Les détails de l'exécution sont résumés ci-dessous :

- Ressources Créées :
  - Adresse IP publique : 163.172.150.21
  - Serveur Scaleway : Cloud8_srv1

- Changements aux Outputs :
  - Adresse IP publique mise à jour : "163.172.150.21"



# Partie 2 :

Installation de DRUPAL

Étape 1. Mettre à jour le système
Commande :
sudo apt-get update -y && sudo apt-get upgrade -y

Étape 2. Installer le serveur Apache
Commande :
sudo apt install apache2 -y
sudo systemctl enable apache2
sudo systemctl start apache2

Commande :
sudo systemctl status apache2

Étape 3. Installer PHP 8.2
Commande :
sudo apt-get install php8.2 php8.2-cli php8.2-common php8.2-imap php8.2-redis php8.2-snmp php8.2-xml php8.2-mysqli php8.2-zip php8.2-mbstring php8.2-curl php8.2-gd libapache2-mod-php -y

Ici on vérifie si PHP a bien été installé en affichant la version.

Commande :
php -v

Étape 4. Installer le service de base de données MariaDB
Ici on installe MariaDB, on le démarre ensuite on l’active  pour qu'il démarre automatiquement avec le système.
Commande :
sudo apt install mariadb-server -y
sudo systemctl enable mariadb
sudo systemctl start mariadb

Afin de voir si MariaDB fonctionne ou est actif on vérifie le statut avec cette commande.
Commande :
sudo systemctl status mariadb

Étape 5. Créer une base de données et un utilisateur Drupal dans MariaDB
On s’est connecté à MariaDB et on a executé les commandes SQL suivantes pour créer la base de données et l'utilisateur Drupal.
Commande :
sudo mysql -u root -p

Commande (SQL) :
CREATE USER 'yosr'@'localhost' IDENTIFIED BY 'userpw';
CREATE DATABASE Cloud8;
GRANT ALL PRIVILEGES ON Cloud8.* TO 'yosr'@'localhost';
FLUSH PRIVILEGES;
EXIT;

Étape 6. Configuration d'Apache pour Drupal
On a créé un fichier de configuration de virtual host pour Apache.
Commande :
sudo nano /etc/apache2/sites-available/
touch drupal.conf
sudo nano /etc/apache2/sites-available/drupal.conf

Sur ce fichier :.
Commande (apache)  :
<br>
<img src="images/drupal.conf.png" width="300"/>
<img src="images/drupal.conf-serveur.png" width="300"/>
<br>


Étape 7. Activer et Redémarrer Apache
Ensuite on active la nouvelle configuration de virtual host et on redémarre Apache.
Commande :
sudo a2ensite drupal.conf
sudo systemctl restart apache2
<br>
<img src="images/terminer.png" width="300"/>



# Partie 3 :

L'architecture : 
<br>
<img src="images/architecture.png" width="300"/>
<br>

Création des 3 instances :

Instance Backend :
Cloud8-serv1
Cloud8-serv2
Instance MariaDB :
Cloud8-serv3
<br>
<img src="images/Instance-creer.png" width="300"/>
<br>
Script Bastion :
Commande :
yosr@debian:~/evaluation/TP_vpc/Tf$ tofu output
bastion_ip = "163.172.132.110"
lb_ip = "195.154.73.177"
srv1_private_ip = "10.76.175.45"
srv2_private_ip = "10.76.175.53"
srv3_private_ip = "10.76.169.17"


Connexion aux Instances via Bastion :
Commande :
sudo ssh -J bastion@163.172.132.110:61000 root@10.76.175.45
sudo ssh -J bastion@163.172.132.110:61000 root@10.76.175.53
sudo ssh -J bastion@163.172.132.110:61000 root@10.76.175.17
<br>
<img src="images/bastion.png" width="300"/>
<br>

Connexion réussie aux instances backend Cloud8-serv1 et Cloud8-serv2.

Configuration de MariaDB sur l'Instance 3 (Cloud8-serv3) :
Commande :
sudo apt update
sudo apt install mariadb-server

Créer la base de données et l'utilisateur pour Drupal :
Commande :
sudo mysql -u root -p
CREATE DATABASE drupaldb CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
CREATE USER 'drupaluser'@'%' IDENTIFIED BY 'userpw';
GRANT ALL PRIVILEGES ON drupaldb.* TO 'drupaluser'@'%';
FLUSH PRIVILEGES;
EXIT;

Modifier la configuration de MariaDB :
Commande :
sudo nano /etc/mysql/mariadb.conf.d/50-server.cnf
sudo systemctl restart mariadb
sudo systemctl status mariadb

Création de 3 Nouvelles Instances (Backend) avec Terraform :
Fichiers Terraform (instance.tf) :
Commande :
yosr@debian:~/evaluation/TP_vpc/Tf$ cat instance.tf

resource "scaleway_instance_server" "srv2" {
   name  = "Cloud8-srv2"
   type  = "PRO2-XXS"
   image = "debian_bookworm"
   tags = [ "Cloud8", "web", "apache2" ]
   root_volume {
     size_in_gb = 10
   }
   user_data = {
      role       = "web"
      cloud-init = file("${path.module}/init_srv2.sh")
   }
   enable_ipv6 = false
   private_network {
      pn_id = scaleway_vpc_private_network.myvpc.id
   }
}

- Seconde instance

resource "scaleway_instance_server" "srv3" {
   name  = "Cloud8-srv3"
   type  = "PRO2-XXS"
   image = "debian_bookworm"
   tags = [ "Cloud8", "web", "apache2" ]
   root_volume {
     size_in_gb = 10
   }
   user_data = {
      role       = "web"
      cloud-init = file("${path.module}/init_srv3.sh")
   }
   enable_ipv6 = false
   private_network {
      pn_id = scaleway_vpc_private_network.myvpc.id
   }
}

resource "scaleway_instance_server" "srv4" {
   name  = "Cloud8-srv4"
   type  = "PRO2-XXS"
   image = "debian_bookworm"
   tags = [ "Cloud8", "web", "apache2" ]
   root_volume {
     size_in_gb = 10
   }
   user_data = {
      role       = "web"
      cloud-init = file("${path.module}/init_srv4.sh")
   }
   enable_ipv6 = false
   private_network {
      pn_id = scaleway_vpc_private_network.myvpc.id
   }
}

- Données en sortie

output "srv2_private_ip" {
 value = "${scaleway_instance_server.srv1.private_ip}"
}

output "srv3_private_ip" {
 value = "${scaleway_instance_server.srv3.private_ip}"
}

output "srv4_private_ip" {
 value = "${scaleway_instance_server.srv4.private_ip}"
}


Script d'initialisation (init_srv4.sh) :
Commande :
#!/usr/bin/env bash

sleep 60
apt -y update > /root/aptu.log 2> /root/aptu.log
apt -y install apache2 > /root/apt.log 2> /root/apt.err.log

touch /root/srv4_cloud-init

reboot

Vérification de la Connexion SSH aux Nouvelles Instances :
Script Terraform pour vérifier la connectivité SSH aux nouvelles instances :
Commande :
./go

on ajoute  une gateway (à créer au préalable) à votre VPC et on active le bastion associé à cette gateway. Ensuite on note l'adresse IP publique de la gateway.
On teste le bastion :
Commande :
ssh -J bastion@212.47.244.24:61000 root@Cloud8-srv1

ssh -J bastion@212.47.244.24:61000 root@Cloud8-srv2

<br>
<img src="images/bastion2.png" width="300"/>
<br>

À ce stade on peut supprimer les ip publiques des deux instances et on vérifie que l'on peut toujours s'y connecter via le bastion.
Restart Mariadb :
Commande :

root@test-2-srv3:~# sudo nano /etc/mysql/mariadb.conf.d/50-server.cnf
<br>
root@test-2-srv3:~# sudo systemctl restart mariadb
<br>
root@test-2-srv3:~# sudo systemctl status mariadb
<br>
<img src="images/IP.png" width="300"/>
<br>

et là quand on met l’adresse ip du load balancer lb_ip on a apache2 qui s’affiche :
<br>
<img src="images/Apachefinie.png" width="300"/>
<br>
